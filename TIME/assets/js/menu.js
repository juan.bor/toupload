$(window).scroll(function() {
    if ($('#menu').offset().top > 150) {
        $('#menu').addClass("menu-color");
    } else {
        $('#menu').removeClass('menu-color')
    }
})

$('#menu a').on('click', function(e) {
    if (this.hash !== '') {
        e.preventDefault();

        const hash = this.hash;

        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800);
    }
})